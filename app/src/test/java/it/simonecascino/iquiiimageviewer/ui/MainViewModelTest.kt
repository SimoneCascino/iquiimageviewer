package it.simonecascino.iquiiimageviewer.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import it.simonecascino.iquiiimageviewer.datalayer.network.utils.ApiStatus
import it.simonecascino.iquiiimageviewer.getOrAwaitValue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
@Config(application = HiltTestApplication::class)
class MainViewModelTest{

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject lateinit var mainRepository: MainRepository

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    fun test_apistatus_handling(){

        val mainViewModel = MainViewModel(mainRepository)
        mainViewModel.handleApiStatus()

        val value = mainViewModel.searchApiStatus.getOrAwaitValue()

        assert(value == ApiStatus.Idle)

    }

}