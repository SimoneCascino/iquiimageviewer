package it.simonecascino.iquiiimageviewer

import it.simonecascino.iquiiimageviewer.models.Thumbnail
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class ThumbnailTest {

    @Test
    fun check_ratio_handle_divided_by_zero(){

        val thumbnail = Thumbnail("https://www.google.it",
            0, 0)

        assertThat(thumbnail.ratio,`is` (1F))

    }



}