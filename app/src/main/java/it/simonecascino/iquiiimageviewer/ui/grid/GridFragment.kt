package it.simonecascino.iquiiimageviewer.ui.grid

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.SearchView
import androidx.core.app.SharedElementCallback
import androidx.core.view.doOnPreDraw
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.transition.TransitionInflater
import androidx.transition.TransitionManager
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import it.simonecascino.iquiiimageviewer.R
import it.simonecascino.iquiiimageviewer.databinding.FragmentGridBinding
import it.simonecascino.iquiiimageviewer.datalayer.network.utils.ApiStatus
import it.simonecascino.iquiiimageviewer.datalayer.network.utils.Reason
import it.simonecascino.iquiiimageviewer.ui.MainViewModel
import it.simonecascino.iquiiimageviewer.utils.hideKeyboard
import it.simonecascino.iquiiimageviewer.utils.observe
import it.simonecascino.iquiiimageviewer.utils.snackbar
import java.util.*

@AndroidEntryPoint
class GridFragment : Fragment() {

    private lateinit var binding: FragmentGridBinding

    private val viewModel: MainViewModel by activityViewModels()

    private var snackbar: Snackbar? = null

    override fun onPause() {
        super.onPause()
        snackbar?.dismiss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_grid, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.galleryView.setHasFixedSize(true)

        binding.galleryView.addOnScrollListener(object: RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if(newState != RecyclerView.SCROLL_STATE_IDLE)
                    recyclerView.hideKeyboard()

            }
        })

        binding.searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{

            override fun onQueryTextChange(text: String?): Boolean {

                snackbar?.dismiss()

                if(text == null) viewModel.getImages("")
                else viewModel.getImages(text)

                return true
            }

            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

        })

        binding.galleryView.post {

            val horizontalSpace = (binding.galleryView.width - (binding.galleryView.paddingStart + binding.galleryView.paddingEnd))/
                    (binding.galleryView.layoutManager as StaggeredGridLayoutManager).spanCount

            val adapter = ImageGridAdapter(ImageGridAdapter.ImageListener(
                { image ->
                    viewModel.updateFavourite(image)
                }, { index, view ->

                    viewModel.pagerIndex = index

                    val extras = FragmentNavigatorExtras(view to view.transitionName)

                    findNavController().navigate(
                        GridFragmentDirections.actionGridFragmentToDetailFragment(index),
                        extras
                    )
                }
            ), horizontalSpace)

            binding.galleryView.adapter = adapter

            observe(viewModel.images){ images ->
                adapter.submitList(images)
            }

            observe(viewModel.enableAnimations){enable ->
                (binding.galleryView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = enable
            }

        }

        observe(viewModel.searchApiStatus){ apiStatus ->

            TransitionManager.beginDelayedTransition(binding.searchLayout)
            binding.progressbar.visibility = if(apiStatus == ApiStatus.Loading)View.VISIBLE else View.GONE

            if(apiStatus is ApiStatus.Fail){

                val message = when(apiStatus.reason){
                    Reason.Unparsable -> getString(R.string.fail_unparsable)
                    Reason.Unreachable -> getString(R.string.fail_unreachable)
                    else -> String.format(Locale.getDefault(), getString(R.string.fail_generic), apiStatus.reason.name)
                }

                snackbar = view.snackbar(message, getString(R.string.snackbar_retry)){
                    viewModel.getImages(binding.searchView.query.toString(), forceRefresh = true)
                }

                viewModel.handleApiStatus()

            }

        }

        setupTransition()

    }

    private fun setupTransition(){

        setExitSharedElementCallback(object : SharedElementCallback() {
            override fun onMapSharedElements(
                names: MutableList<String>,
                sharedElements: MutableMap<String, View>
            ) {
                val position = viewModel.pagerIndex

                val selectedViewHolder = binding.galleryView.findViewHolderForAdapterPosition(position)

                selectedViewHolder?.itemView?.findViewById<ImageView>(R.id.imageView)?.let {
                    sharedElements[names[0]] = it
                }
            }
        })

        postponeEnterTransition()
        binding.galleryView.doOnPreDraw {
            binding.galleryView.scrollToPosition(viewModel.pagerIndex)
            startPostponedEnterTransition()
        }

    }

}