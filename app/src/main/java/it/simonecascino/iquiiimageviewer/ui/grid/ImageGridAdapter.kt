package it.simonecascino.iquiiimageviewer.ui.grid

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import it.simonecascino.iquiiimageviewer.databinding.ItemImageBinding
import it.simonecascino.iquiiimageviewer.models.RedditImage

class ImageGridAdapter(private val callback: ImageListener, private val horizontalSpace: Int): ListAdapter<RedditImage, ImageGridAdapter.ImageViewHolder>(ImageDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ImageViewHolder.from(parent)

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(getItem(position), horizontalSpace, callback)
    }

    class ImageViewHolder private constructor(private val binding: ItemImageBinding): RecyclerView.ViewHolder(binding.root){

        companion object{
            fun from(parent: ViewGroup): ImageViewHolder = ImageViewHolder(ItemImageBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
        }

        fun bind(redditImage: RedditImage, horizontalSpace: Int, callback: ImageListener){
            binding.horizontalSpace = horizontalSpace - (binding.parent.marginStart + binding.parent.marginEnd)
            binding.image = redditImage
            binding.callback = callback
            binding.index = adapterPosition
            binding.executePendingBindings()
        }

    }

    class ImageListener(private val starClicked: (RedditImage) -> Unit, private val clicked: (Int, View) -> Unit){
        fun onStarClick(image: RedditImage) = starClicked(image)
        fun onImageClick(index: Int, view: View) = clicked(index, view)
    }

}

private class ImageDiffUtil: DiffUtil.ItemCallback<RedditImage>(){

    override fun areItemsTheSame(oldItem: RedditImage, newItem: RedditImage) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: RedditImage, newItem: RedditImage): Boolean{

        return oldItem.isFavourite == newItem.isFavourite &&
                oldItem.thumbnail == newItem.thumbnail &&
                oldItem.url == newItem.url

    }

}