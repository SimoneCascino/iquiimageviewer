package it.simonecascino.iquiiimageviewer.ui

import android.util.Log
import androidx.lifecycle.*
import it.simonecascino.iquiiimageviewer.datalayer.database.daos.ImageDao
import it.simonecascino.iquiiimageviewer.datalayer.database.entities.RedditImageDb
import it.simonecascino.iquiiimageviewer.datalayer.database.entities.toDomainModel
import it.simonecascino.iquiiimageviewer.datalayer.database.utils.prettyString
import it.simonecascino.iquiiimageviewer.datalayer.network.Api
import it.simonecascino.iquiiimageviewer.datalayer.network.pojos.toDbModel
import it.simonecascino.iquiiimageviewer.datalayer.network.utils.ApiStatus
import it.simonecascino.iquiiimageviewer.models.RedditImage
import kotlinx.coroutines.CancellationException
import javax.inject.Inject
import javax.inject.Singleton

class MainRepository @Inject constructor(
    private val imageDao: ImageDao,
    private val api: Api,
    private val liveTextQuery: MutableLiveData<String>,
    private val apiStatus: MutableLiveData<ApiStatus>
){

    private val _images = ImagesMediator(
        liveTextQuery,
        imageDao
    )

    val enableAnimations = liveTextQuery.map {
        it.isBlank()
    }

    val images: LiveData<List<RedditImage>>
        get() = _images

    val searchApiStatus = Transformations.distinctUntilChanged(apiStatus)

    suspend fun getImages(text: String, forceRefresh: Boolean){

        if(liveTextQuery.value == text && !forceRefresh)
            return

        liveTextQuery.postValue(text)

        if(!text.isBlank()){

            try{

                apiStatus.postValue(ApiStatus.Loading)

                val redditResponse = api.getImages(text)

                val operations = imageDao.upsertImages(redditResponse.toDbModel())

                Log.d("app_db", operations.prettyString())

                apiStatus.postValue(ApiStatus.Success)

            }catch (e: Exception){
                if(e !is CancellationException){
                    e.printStackTrace()
                    apiStatus.postValue(ApiStatus.Fail(e))
                }
            }
        }

    }

    suspend fun updateFavourite(image: RedditImage){
        imageDao.updateFavourite(image.id, !image.isFavourite)
    }

    fun handleApiStatus(){
        apiStatus.value = ApiStatus.Idle
    }

}

class ImagesMediator @Inject constructor(val text: MutableLiveData<String>, imageDao: ImageDao): MediatorLiveData<List<RedditImage>>(){

    private var previousText = text.value

    private lateinit var images: LiveData<List<RedditImageDb>>

    init {

        addSource(text){ queryText ->

            if(queryText != previousText){

                previousText = queryText

                if(::images.isInitialized)
                    removeSource(images)

                images = if(queryText.isBlank()){
                    imageDao.getFavourites()
                }

                else imageDao.searchImages(queryText)

                addSource(images){

                    val obtainedValues = it.toDomainModel()

                    if(value!=obtainedValues)
                        value = obtainedValues
                }

            }

        }

        text.value = ""

    }

}