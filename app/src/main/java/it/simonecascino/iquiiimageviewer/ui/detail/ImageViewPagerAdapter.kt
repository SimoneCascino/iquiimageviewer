package it.simonecascino.iquiiimageviewer.ui.detail

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class ImageViewPagerAdapter(activity: FragmentActivity): FragmentStateAdapter(activity) {


    var fragments: List<DetailFragment>? = null
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun getItemCount() = fragments?.size ?: 0

    override fun createFragment(position: Int) = fragments!![position]


}