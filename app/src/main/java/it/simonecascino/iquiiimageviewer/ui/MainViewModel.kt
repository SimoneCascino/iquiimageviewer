package it.simonecascino.iquiiimageviewer.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.simonecascino.iquiiimageviewer.models.RedditImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(private val mainRepository: MainRepository): ViewModel(){

    var pagerIndex = 0

    val images = mainRepository.images

    val enableAnimations = mainRepository.enableAnimations

    val searchApiStatus = mainRepository.searchApiStatus

    private lateinit var job: Job

    fun getImages(text: String, forceRefresh: Boolean = false){

        if(::job.isInitialized && job.isActive)
            job.cancel()

        job = viewModelScope.launch(Dispatchers.IO){
            delay(500)
            mainRepository.getImages(text, forceRefresh)
        }

    }

    fun updateFavourite(image: RedditImage){

        viewModelScope.launch {
            mainRepository.updateFavourite(image)
        }

    }

    fun handleApiStatus(){
        mainRepository.handleApiStatus()
    }

}