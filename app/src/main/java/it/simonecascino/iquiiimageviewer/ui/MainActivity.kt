package it.simonecascino.iquiiimageviewer.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import dagger.hilt.android.AndroidEntryPoint
import it.simonecascino.iquiiimageviewer.R
import it.simonecascino.iquiiimageviewer.databinding.ActivityMainBinding
import it.simonecascino.iquiiimageviewer.utils.hideKeyboard

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_main
        )

        setSupportActionBar(binding.toolbar)

        NavigationUI.setupWithNavController(binding.toolbar, findNavController(R.id.navHost))

        findNavController(R.id.navHost).addOnDestinationChangedListener { _, destination, _ ->

            if(destination.id == R.id.detailHostFragment){
                binding.appBarLayout.apply {
                    setExpanded(true)
                    hideKeyboard()
                }
            }

        }

    }
}