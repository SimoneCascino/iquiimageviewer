package it.simonecascino.iquiiimageviewer.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.app.SharedElementCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import androidx.viewpager2.widget.ViewPager2
import dagger.hilt.android.AndroidEntryPoint
import it.simonecascino.iquiiimageviewer.R
import it.simonecascino.iquiiimageviewer.databinding.FragmentDetailHostBinding
import it.simonecascino.iquiiimageviewer.ui.MainViewModel
import it.simonecascino.iquiiimageviewer.utils.observe

@AndroidEntryPoint
class DetailHostFragment : Fragment() {

    private lateinit var binding: FragmentDetailHostBinding

    private val viewModel: MainViewModel by activityViewModels()

    private val args: DetailHostFragmentArgs by navArgs()

    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        postponeEnterTransition()
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)

        setEnterSharedElementCallback(object : SharedElementCallback() {
            override fun onMapSharedElements(
                names: MutableList<String>,
                sharedElements: MutableMap<String, View>
            ) {

                val position = viewModel.pagerIndex

                binding.pager.findViewWithTag<ViewGroup>(position)
                    ?.findViewById<ImageView>(R.id.imageView)?.let { sharedElements[names[0]] = it }
            }
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail_host, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ImageViewPagerAdapter(requireActivity())

        binding.pager.adapter = adapter

        observe(viewModel.images){images ->

            val fragments = images.mapIndexed { index, redditImage ->
                DetailFragment.newInstance(index, redditImage)
            }

            if(adapter.fragments == null){

                adapter.fragments = fragments

                fragments[args.index].loaded = {
                    startPostponedEnterTransition()
                }

                binding.pager.setCurrentItem(args.index, false)

                binding.image = images[args.index]

            }

            else adapter.fragments = fragments

        }

        binding.pager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback(){

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                viewModel.pagerIndex = position

                viewModel.images.value?.let {images ->
                    binding.image = images[position]
                }

            }
        })

    }


}