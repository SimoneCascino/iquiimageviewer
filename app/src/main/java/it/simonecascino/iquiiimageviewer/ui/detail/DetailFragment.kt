package it.simonecascino.iquiiimageviewer.ui.detail

import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import it.simonecascino.iquiiimageviewer.R
import it.simonecascino.iquiiimageviewer.databinding.FragmentDetailBinding
import it.simonecascino.iquiiimageviewer.models.RedditImage

private const val KEY_IMAGE = "image"
private const val KEY_INDEX = "index"

class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding

    lateinit var loaded: () -> Unit

    companion object{
        fun newInstance(index: Int, image: RedditImage) = DetailFragment().apply {
            arguments = Bundle(1).apply {
                putParcelable(KEY_IMAGE, image)
                putInt(KEY_INDEX, index)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {

            val image: RedditImage? = it.getParcelable(KEY_IMAGE)
            binding.image = image
            binding.index = it.getInt(KEY_INDEX)
            binding.loadListener = object: RequestListener<Drawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    if(::loaded.isInitialized)
                        loaded()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    if(::loaded.isInitialized)
                        loaded()
                    return false
                }

            }

        }

    }

    /*

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.menu_detail, menu)

    }

     */

}