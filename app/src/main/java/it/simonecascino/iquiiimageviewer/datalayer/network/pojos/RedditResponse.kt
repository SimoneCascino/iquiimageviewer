package it.simonecascino.iquiiimageviewer.datalayer.network.pojos

import android.os.Build
import android.text.Html
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import it.simonecascino.iquiiimageviewer.datalayer.database.entities.RedditImageDb

@JsonClass(generateAdapter = true)
data class RedditResponse (
    val data: DataContainerResponse
)

data class DataContainerResponse(
    val children: List<ChildrenResponse>
)

data class ChildrenResponse(
    val data: DataResponse
)

data class DataResponse(
    val id: String,
    val subreddit: String,
    val title: String,
    @Json(name = "subreddit_name_prefixed") val subredditNamePrefixed: String,
    val score: Int,
    @Json(name = "is_video") val isVideo: Boolean,
    val permalink: String,
    val url: String,
    val created: Double,
    @Json(name = "num_comments") val numComments: Int,
    val preview: PreviewResponse?
)

data class PreviewResponse(
    val images: List<ImageResponse>
)

data class ImageResponse(
    val resolutions: List<ResolutionResponse>
)

data class ResolutionResponse(
    val url: String,
    val width: Int,
    val height: Int
){
    val validUrl = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(url, Html.FROM_HTML_MODE_LEGACY).toString()
    } else {
        Html.fromHtml(url).toString()
    }
}


fun RedditResponse.toDbModel(): List<RedditImageDb>{

    return data.children.map { childrenResponse ->

        val data = childrenResponse.data

        val thumbnail = data.preview?.images?.first()?.resolutions?.minBy {
            it.width
        }

        RedditImageDb(
            data.id,
            data.subreddit,
            data.title,
            data.subredditNamePrefixed,
            data.score,
            data.isVideo,
            data.permalink,
            data.url,
            data.created,
            data.numComments,
            thumbnail?.validUrl,
            thumbnail?.width,
            thumbnail?.height
        )

    }
}


