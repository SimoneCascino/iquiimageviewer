package it.simonecascino.iquiiimageviewer.datalayer.database.utils

import kotlin.reflect.KClass

data class Operation<T: Any>(
    val name: KClass<T>,
    val type: Type,
    val value: Int
){

    override fun toString(): String {
        return "${type.name} ${name.simpleName} -> $value"
    }

    enum class Type{
        Insert, Update, Delete
    }

}

fun List<Operation<*>>.prettyString(): String{

    val builder = StringBuilder()

    forEach {
        builder.append(it.toString())
        builder.append("\n")
    }

    builder.removeSuffix("\n")

    return builder.toString()
}
