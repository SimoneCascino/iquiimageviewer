package it.simonecascino.iquiiimageviewer.datalayer.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import it.simonecascino.iquiiimageviewer.models.RedditImage
import it.simonecascino.iquiiimageviewer.models.Thumbnail

@Entity(tableName = "images")
data class RedditImageDb (
    @PrimaryKey val id: String,
    val subreddit: String,
    val title: String,
    @ColumnInfo(name = "subreddit_name_prefixed") val subredditNamePrefixed: String,
    val score: Int,
    @ColumnInfo(name = "is_video") val isVideo: Boolean,
    val permalink: String,
    val url: String,
    val created: Double,
    @ColumnInfo(name = "num_comments") val numComments: Int,
    val thumbnail: String?,
    @ColumnInfo(name = "thumbnail_width") val thumbnailWidth: Int?,
    @ColumnInfo(name = "thumbnail_height") val thumbnailHeight: Int?,
    @ColumnInfo(name = "is_favourite") val isFavourite: Boolean = false
)

data class PartialRedditImageDb(
    val id: String,
    val subreddit: String,
    val title: String,
    @ColumnInfo(name = "subreddit_name_prefixed") val subredditNamePrefixed: String,
    val score: Int,
    @ColumnInfo(name = "is_video") val isVideo: Boolean,
    val permalink: String,
    val url: String,
    val created: Double,
    @ColumnInfo(name = "num_comments") val numComments: Int,
    val thumbnail: String?,
    @ColumnInfo(name = "thumbnail_width") val thumbnailWidth: Int?,
    @ColumnInfo(name = "thumbnail_height") val thumbnailHeight: Int?
)

fun RedditImageDb.toPartial(): PartialRedditImageDb{

    return PartialRedditImageDb(
        id,
        subreddit,
        title,
        subredditNamePrefixed,
        score,
        isVideo,
        permalink,
        url,
        created,
        numComments,
        thumbnail,
        thumbnailWidth,
        thumbnailHeight
    )

}

fun List<RedditImageDb>.toDomainModel(): List<RedditImage>{

    return map {

        var thumbnail: Thumbnail? = null

        if(it.thumbnail!=null){
            thumbnail = Thumbnail(it.thumbnail, it.thumbnailWidth ?: 1, it.thumbnailHeight ?: 1)
        }

        RedditImage(
            it.id,
            it.title,
            it.subredditNamePrefixed,
            it.score,
            it.isVideo,
            it.permalink,
            it.url,
            it.created,
            it.numComments,
            thumbnail,
            it.isFavourite
        )

    }

}