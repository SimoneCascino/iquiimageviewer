package it.simonecascino.iquiiimageviewer.datalayer.network

import it.simonecascino.iquiiimageviewer.datalayer.network.pojos.RedditResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {

    @GET("{text}/top.json")
    suspend fun getImages(@Path("text") text: String): RedditResponse

}