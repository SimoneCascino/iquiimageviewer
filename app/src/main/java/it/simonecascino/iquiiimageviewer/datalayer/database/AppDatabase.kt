package it.simonecascino.iquiiimageviewer.datalayer.database

import androidx.room.Database
import androidx.room.RoomDatabase
import it.simonecascino.iquiiimageviewer.datalayer.database.daos.ImageDao
import it.simonecascino.iquiiimageviewer.datalayer.database.entities.RedditImageDb

@Database(entities = [RedditImageDb::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase(){

    abstract fun imageDao(): ImageDao

}