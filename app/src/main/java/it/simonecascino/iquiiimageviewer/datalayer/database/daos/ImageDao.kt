package it.simonecascino.iquiiimageviewer.datalayer.database.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import it.simonecascino.iquiiimageviewer.datalayer.database.entities.PartialRedditImageDb
import it.simonecascino.iquiiimageviewer.datalayer.database.entities.RedditImageDb
import it.simonecascino.iquiiimageviewer.datalayer.database.entities.toPartial
import it.simonecascino.iquiiimageviewer.datalayer.database.utils.Operation

@Dao
interface ImageDao{

    @Query("select * from images where subreddit like '%'||:text||'%'")
    fun searchImages(text: String): LiveData<List<RedditImageDb>>

    @Query("select * from images where is_favourite = 1")
    fun getFavourites(): LiveData<List<RedditImageDb>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertImages(redditImages: List<RedditImageDb>): List<Long>

    @Update(entity = RedditImageDb::class)
    suspend fun updateImages(redditImages: List<PartialRedditImageDb>): Int

    @Query("update images set is_favourite = :favourite where id = :id")
    suspend fun updateFavourite(id: String, favourite: Boolean)

    @Transaction
    suspend fun upsertImages(redditImages: List<RedditImageDb>): List<Operation<RedditImageDb>>{

        val operations = ArrayList<Operation<RedditImageDb>>()

        val toUpdate = ArrayList<PartialRedditImageDb>()

        var count = 0

        insertImages(redditImages).forEachIndexed { index, id ->
            if(id == -1L) toUpdate.add(redditImages[index].toPartial())
            else count ++
        }

        operations.add(Operation(RedditImageDb::class, Operation.Type.Insert, count))

        count = updateImages(toUpdate)

        operations.add(Operation(RedditImageDb::class, Operation.Type.Update, count))

        return operations

    }

}