package it.simonecascino.iquiiimageviewer.utils

import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar

inline fun <T> LifecycleOwner.observe(liveData: LiveData<T>, crossinline observer: (T) -> Unit){

    val observerOwner = if(this is Fragment)
        viewLifecycleOwner else this

    liveData.observe(observerOwner, Observer{
        observer(it)
    })
}

fun Context.dpToPx(dp: Int): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics)
        .toInt()
}

fun View.hideKeyboard(){
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(applicationWindowToken, 0)
}

inline fun View.snackbar(message: String, action: String, crossinline actionCallback: () -> Unit): Snackbar {

    return Snackbar.make(this, message, Snackbar.LENGTH_INDEFINITE)
        .setAction(action){
            actionCallback()
        }.apply {
            animationMode = Snackbar.ANIMATION_MODE_SLIDE
            show()
        }

}