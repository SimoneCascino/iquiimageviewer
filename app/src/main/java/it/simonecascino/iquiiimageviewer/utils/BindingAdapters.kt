package it.simonecascino.iquiiimageviewer.utils

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import it.simonecascino.iquiiimageviewer.R
import it.simonecascino.iquiiimageviewer.models.RedditImage


@BindingAdapter("loadImage", "horizontalSpace")
fun ImageView.loadImage(redditImage: RedditImage?, horizontalSpace: Int = 0){

    redditImage?.let {

        it.thumbnail?.let { thumbnail ->

            val url = thumbnail.url
            val preloadedHeight = horizontalSpace/thumbnail.ratio

            layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                if(preloadedHeight == 0F)FrameLayout.LayoutParams.WRAP_CONTENT else preloadedHeight.toInt())

            Glide.with(this).load(url)
                .error(R.drawable.image_error)
                .thumbnail(Glide.with(this).load(R.drawable.loading))
                .into(this)

        } ?: run{

            Glide.with(this)
                .load(it.url)
                .error(R.drawable.image_error)
                .thumbnail(Glide.with(this).load(R.drawable.loading))
                .into(this)

        }

    }

}

@BindingAdapter("loadFullImage", "loadListener")
fun ImageView.loadImage(redditImage: RedditImage?, loadListener: RequestListener<Drawable>?){

    redditImage?.let {

        val glideRequest = Glide.with(this).load(it.url).apply(RequestOptions.noAnimation()).error(R.drawable.image_error)

        if(it.thumbnail!=null){
            val thumbnailRequest: RequestBuilder<Drawable> = Glide.with(context).load(it.thumbnail.url).addListener(loadListener)
            glideRequest.thumbnail(thumbnailRequest).into(this)
        }

        else glideRequest.addListener(loadListener).into(this)

    }

}

@BindingAdapter("isSelected")
fun View.isSelected(selected: Boolean?){
    selected?.let {
        isSelected = selected
    }
}