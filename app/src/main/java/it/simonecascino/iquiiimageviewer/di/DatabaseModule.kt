package it.simonecascino.iquiiimageviewer.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import it.simonecascino.iquiiimageviewer.datalayer.database.AppDatabase
import it.simonecascino.iquiiimageviewer.datalayer.database.daos.ImageDao
import javax.inject.Singleton

private const val DB_NAME = "IQUII.db"

@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule{

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            DB_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideDao(appDatabase: AppDatabase): ImageDao{
        return appDatabase.imageDao()
    }

}