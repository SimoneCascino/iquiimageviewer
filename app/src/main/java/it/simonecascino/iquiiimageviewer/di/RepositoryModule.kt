package it.simonecascino.iquiiimageviewer.di

import androidx.lifecycle.MutableLiveData
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import it.simonecascino.iquiiimageviewer.datalayer.database.daos.ImageDao
import it.simonecascino.iquiiimageviewer.datalayer.network.Api
import it.simonecascino.iquiiimageviewer.ui.MainRepository
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(imageDao: ImageDao,
                          api: Api): MainRepository{
        return MainRepository(imageDao, api, MutableLiveData(), MutableLiveData())
    }

}