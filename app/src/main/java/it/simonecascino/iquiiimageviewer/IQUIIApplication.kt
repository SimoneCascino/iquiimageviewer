package it.simonecascino.iquiiimageviewer

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class IQUIIApplication : Application()