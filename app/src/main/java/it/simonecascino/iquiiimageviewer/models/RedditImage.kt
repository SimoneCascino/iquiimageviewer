package it.simonecascino.iquiiimageviewer.models

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RedditImage (
    val id: String,
    val title: String,
    val subredditPrefix: String,
    val score: Int,
    val isVideo: Boolean,
    val permalink: String,
    val url: String,
    val created: Double,
    val numComments: Int,
    val thumbnail: Thumbnail?,
    val isFavourite: Boolean
) : Parcelable

@Parcelize
data class Thumbnail(
    val url: String,
    val width: Int,
    val height: Int
) : Parcelable {
    @IgnoredOnParcel val ratio = if(height == 0) 1F else width.toFloat()/height.toFloat()
}