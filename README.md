# README #

Un paio di note utili alla valutazione:

* Ho usato l'api fornita. Non ho studiato le funzionalità di reddit, ad esempio non so se c'è la possibilità di paginare i risultati e per questo non ho usato paging.
* In alcuni casi il json che ricevo i campi ottenuti non sono gli stessi, ho dato priorità allo sviluppo dell'app in sè, sorvolando sulle ambiguità (che sono comunque gestite)
* L'app fornisce la possibilità di cercare foto e di markarle come preferite. Dal momento che uso room come cache, che è la mia "source of truth" ho pensato che invece di avere 2 schermate diverse, potevo semplicemente applicare un filtro differente sul mio db. Dunque, se il campo di ricerca è vuoto, mostro solo i preferiti, altrimenti mostro i risultati per la ricerca.
* Non so sulla base di quale campo viene effettuata la ricerca, io la faccio sul valore "subreddit" del json, ho pensato che ai fini della valutazione non fosse importante.
* Per la DI sto usando Hilt, una libreria nuova, ufficiale Google, sviluppata su Dagger, ma è la prima volta che la uso.
* Non ho avuto tempo per i test, che ho appena iniziato, ma mi sono concentrato sullo sviluppo dell'app.

Sarò ben lieto di motivare alcune delle mie scelte in un colloquio tecnico.